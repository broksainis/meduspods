class Node {
    constructor(name, weight) {
        this.name = name;
        this.weight = weight;
    }
}

class Graph {
    constructor(vertices) {
        this.adjacencyList = new Map();
        for (let i = 1; i <= vertices; i++) {
            this.adjacencyList.set(i, []);
        }
        this.allPaths = [];
    }

    addEdge(vertexFrom, vertexTo, weight) {
        const vFrom = this.adjacencyList.get(vertexFrom);
        const vTo = this.adjacencyList.get(vertexTo);
        if (!vFrom.includes(vertexTo) && !vTo.includes(vertexFrom)) {
            vFrom.push(new Node(vertexTo, weight));
            vTo.push(new Node(vertexFrom, weight));

        }
        // if (!vTo.includes(vertexFrom)) {
        //     vTo.push(new Node(vertexFrom, weight));
        // }
    }

    getPathsFromTo(start) {
        const children = this.adjacencyList.get(start);
        const path = new Array();
        children.map(child => {
            this.getAllPaths(child.name, start, path, start);
        });
    }

    getAllPaths(vertexFrom, vertexTo, paths, start) {
        paths.push(vertexFrom);
        if (vertexFrom === vertexTo) {
            this.allPaths.push(JSON.parse(JSON.stringify(paths)));
            return;
        }
        const children = this.adjacencyList.get(vertexFrom);
        children.map(child => {
            // make sure path includes not already visited vertex and it doesn't count parent as child
            if (!paths.includes(child.name) && child.name !== start) {
                const newPath = new Array();
                paths.map((vertexInPaths, i) => {
                    newPath.push(vertexInPaths);
                })
                this.getAllPaths(child.name, vertexTo, newPath);
            }
        })
    }
}

const G1 = new Graph(5);
G1.addEdge(1, 2, 7);
G1.addEdge(2, 4, 9);
G1.addEdge(2, 3, 100);
G1.addEdge(3, 4, -66);
G1.addEdge(4, 5, 6);
G1.addEdge(5, 1, 2);
// console.log(G1.adjacencyList)


const paths = new Array();
G1.getPathsFromTo(1);
// code above print out [ [ 2, 4, 5, 1 ], [ 2, 3, 4, 5, 1 ] ]
console.log(G1.allPaths);

